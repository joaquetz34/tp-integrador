using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPowerUp : MonoBehaviour
{
    public float multiplier = 4.0f;
    public GameObject pickupEffect;
    public float duration = 4f;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
           StartCoroutine (PickUp(other) );
        }
    }

    IEnumerator PickUp(Collider Jugador)
    {
        Instantiate(pickupEffect, transform.position, transform.rotation);

        ControlJugador controljugador = Jugador.GetComponent<ControlJugador>();
        controljugador.vida *= multiplier;
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

        yield return new WaitForSeconds(duration);

        controljugador.vida /= multiplier;
        Destroy(gameObject);
    }
}
