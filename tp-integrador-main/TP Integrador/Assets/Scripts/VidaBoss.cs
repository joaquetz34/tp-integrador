using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaBoss : MonoBehaviour
{
    public float vida;
    public GameObject explosion;
    public GameObject cartel;
    void Start()
    {
        
    }

    void Update()
    {
        if (vida <= 0)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            cartel.SetActive(true);
            Destroy(gameObject);
        }
    }
}
