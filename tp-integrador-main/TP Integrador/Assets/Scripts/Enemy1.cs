using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy1 : MonoBehaviour
{

    private GameObject Jugador;
    public int rapidez;
  
    void Start()
    {
        Jugador = GameObject.Find("Jugador");
    }

    void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Jugador")
        {

            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
    }

}
