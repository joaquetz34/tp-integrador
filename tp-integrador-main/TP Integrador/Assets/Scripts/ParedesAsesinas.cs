using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ParedesAsesinas : MonoBehaviour
{

    bool tengoQueMoverme = false;
   public int rapidez = 5;
    public GameObject Jugador;  
    void Update()
    {
        if (transform.position.x <= 4346)
        {
            tengoQueMoverme = true;
        }
        if (transform.position.x >= 4511)
        {
            tengoQueMoverme = false;
        }

        if (tengoQueMoverme)
        {
            MoverIzquierda();
        }
        else
        {
            MoverDerecha();
        }

    }

    void MoverIzquierda()
    {
           transform.position += transform.right * rapidez * Time.deltaTime; 
       // transform.Translate(4262, 127, -1987);
    }


    void MoverDerecha()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
      // transform.Translate(4510, 127, -1987);
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Jugador")
        {

            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
    }
}
