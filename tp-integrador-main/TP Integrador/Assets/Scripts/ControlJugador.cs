using System.Collections;
using System.Collections.Generic; 
using UnityEngine;
using UnityEngine.SceneManagement;


public class ControlJugador : MonoBehaviour

{
    public float rapidezDesplazamiento = 10.0f;
    public float AgregarVelocidad = 1.5f;
    public float RestarVelocidad = 1.5f;
    Vector3 escalaNormal;
    Vector3 escalaAgachado;
    bool agachado;
    public float vida;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        escalaNormal = transform.localScale;
        escalaAgachado = escalaNormal;
        escalaAgachado.y = escalaNormal.y * .75f;
        
    }

    void Example()
    {
        Physics.gravity = new Vector3(0, -35.0F, 0);
    }
    void Update()
    {

        if(vida <= 0)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }

        agachado = Input.GetKey(KeyCode.LeftControl);
        transform.localScale = agachado ? escalaAgachado : escalaNormal;
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
            rapidezDesplazamiento = rapidezDesplazamiento * AgregarVelocidad;
              if (Input.GetKeyDown(KeyCode.LeftAlt))
            rapidezDesplazamiento = rapidezDesplazamiento / RestarVelocidad;
    }
     void FixedUpdate()
    {
        Example();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp") == true)
        {
            this.transform.localScale = new Vector3(transform.localScale.x * 2, transform.localScale.y * 2, transform.localScale.z * 2);
            rapidezDesplazamiento *= 2;
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("fuego") == true)
        {

            StartCoroutine (Da�oFuego());
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("fuego") == true)
        {

            StopCoroutine(Da�oFuego());
        }
    }
    
    IEnumerator Da�oFuego()
    {
      
        vida -= 40;
        yield return new WaitForSeconds(1f);
    }

}