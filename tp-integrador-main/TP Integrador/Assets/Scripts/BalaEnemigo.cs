using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEnemigo : MonoBehaviour
{
    public float Velocidad;
    

    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * Velocidad;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            ControlJugador Jugador = other.gameObject.GetComponent<ControlJugador>();
            if (Jugador != null)
            {
                Jugador.vida -= 10;
                Destroy(gameObject);
            }
        }

    }
}
