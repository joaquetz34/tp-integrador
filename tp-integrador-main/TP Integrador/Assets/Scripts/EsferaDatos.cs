using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsferaDatos : MonoBehaviour
{
    public GameObject esfera;
    public Transform mano;
    public float fuerza;
    private bool activo;
    private bool enMano;
    private Vector3 escala;


   private void Start()
    {
        escala = esfera.transform.localScale;
    }
    void Update()
    {
        if (activo == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
               esfera.transform.SetParent(mano);
                esfera.transform.position = mano.position;
                esfera.transform.rotation = mano.rotation;
                esfera.GetComponent<Rigidbody>().isKinematic = true;
                enMano = true;
            }    
            }
        if (Input.GetMouseButtonDown(1))
        {
            esfera.transform.SetParent(null);
            esfera.GetComponent<Rigidbody>().isKinematic = false;
            esfera.transform.localScale = escala;
            if(enMano == true)
            {
                esfera.GetComponent<Rigidbody>().AddForce(transform.forward * fuerza, ForceMode.Impulse);
                enMano = false;
            }
        }

        if(Input.GetKeyDown(KeyCode.G))
        {
            esfera.transform.SetParent(null);
            esfera.GetComponent<Rigidbody>().isKinematic = false;
            esfera.transform.localScale = escala;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Jugador")
        {
            activo = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Jugador")
        {
            activo = false;
        }
    }
}
