using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemigo : MonoBehaviour
{

    public float velocidad;
    public float range;
    public ControlJugador Jugador;
    public bool enemigoCerca;
    void Start()
    {

    }

    void Update()
    {
        if (Vector3.Distance(transform.position, Jugador.transform.position) <= range)
        {
            enemigoCerca = true;
            transform.LookAt(Jugador.transform);
            transform.position += transform.forward * velocidad * Time.deltaTime;
        }
        else
        {
            enemigoCerca = false;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
