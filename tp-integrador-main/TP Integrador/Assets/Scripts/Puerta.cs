using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{

    public static bool puertaLlave;
    public bool abrir;
    public bool cerrar;
    public bool inTrigger;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        if (inTrigger)
        {
            if (cerrar)
            {
                if (puertaLlave)
                {
                    if (Input.GetKeyDown(KeyCode.T))
                    {
                        abrir = true;
                        cerrar = false;
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.T))
                {
                    cerrar = true;
                    abrir = false;
                }
            }
        }

        if (abrir)
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, -90.0f, 0.0f), Time.deltaTime * 200);
            transform.rotation = newRot;
        }
        else
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, 0.0f, 0.0f), Time.deltaTime * 200);
            transform.rotation = newRot;
        }
    }

    void OnGUI()
    {
        if (inTrigger)
        {
            if (abrir)
            {
                GUI.Box(new Rect(0, 0, 200, 25), "Presiona la tecla T para cerrar");
            }
            else
            {
                if (puertaLlave)
                {
                    GUI.Box(new Rect(0, 0, 200, 25), "Presiona la tecla T para abrir");
                }
                else
                {
                    GUI.Box(new Rect(0, 0, 200, 25), "íNecesitas una llave!");
                }
            }
        }
    }
}
