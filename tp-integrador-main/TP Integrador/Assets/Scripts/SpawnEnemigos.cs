using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemigos : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabEnemigo;
    private GameObject _enemigo;
      
    void Update()
    {
        if (_enemigo == null)
        {
            _enemigo = Instantiate(prefabEnemigo) as GameObject;
            _enemigo.transform.position = new Vector3(4190, 80, -1301);
            float angulo = Random.Range(0, 360);
            _enemigo.transform.Rotate(0, angulo, 0);

          
        }
    }
}
