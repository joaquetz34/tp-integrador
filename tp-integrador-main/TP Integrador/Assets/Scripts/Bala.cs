using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour


{
    public float Velocidad;
   
    void Update()
    {
        transform.position += transform.forward*Time.deltaTime*Velocidad;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy1"))
        {
            Destroy(other.gameObject);
            
        }
        if (other.gameObject.CompareTag("Boss"))
        {
            VidaBoss boss = other.gameObject.GetComponent<VidaBoss>();
            if (boss != null)
            {
                boss.vida -= 5;
                Destroy(gameObject);
            }
        }    
    }
}

