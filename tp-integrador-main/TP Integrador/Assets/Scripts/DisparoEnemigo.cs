using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoEnemigo : MonoBehaviour
{

    public GameObject Bala;
    public MovimientoEnemigo enemigo;
    private float tiempoParaDisparar;
    public float tiempoInicial;
    void Start()
    {
        tiempoParaDisparar = tiempoInicial;
    }

    void Update()
    {
        if (enemigo.enemigoCerca == true)
        {
            if (tiempoParaDisparar <= 0)
            {
                tiempoParaDisparar = tiempoInicial;
                Instantiate(Bala, transform.position, transform.rotation);
            }
            else
            {
                tiempoParaDisparar -= Time.deltaTime;
            }    
        }

    }
}
