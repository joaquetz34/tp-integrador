using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfectoSonido : MonoBehaviour
{

    private AudioSource audioSource;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador") == true) 
        {
            audioSource.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
    
            if (other.CompareTag("Jugador") == false)
        {
            audioSource.Stop();
        }

    }

}
