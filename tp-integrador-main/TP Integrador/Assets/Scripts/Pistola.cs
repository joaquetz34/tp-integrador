using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistola : MonoBehaviour
{
    public GameObject Bala;
    public float TiempoParaDisparar;
    private float TiempoInicial;
    public Transform PuntaDePistola;

    void Start()
    {
        TiempoInicial = TiempoParaDisparar;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (TiempoParaDisparar <= 0)
        {

            if (Input.GetMouseButtonDown(0))
            {
                TiempoParaDisparar = TiempoInicial;
                Instantiate(Bala, PuntaDePistola.position, PuntaDePistola.rotation);
            }
        }
        else
        {
            TiempoParaDisparar -= Time.deltaTime;
        }

               
    }
}
