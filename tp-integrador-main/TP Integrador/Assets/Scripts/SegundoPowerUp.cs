using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegundoPowerUp : MonoBehaviour
{

    public float multiplier = 4.0f;
    public GameObject pickupEffect;

   
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            PickUp(other);

        }
    }

   void PickUp(Collider Jugador)
    {
        Instantiate(pickupEffect, transform.position, transform.rotation);

        ControlJugador controljugador = Jugador.GetComponent<ControlJugador>();
        controljugador.vida *= multiplier;
     
        Destroy(gameObject);
    }
}


