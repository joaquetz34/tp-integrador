using System.Collections.Specialized;
using UnityEngine;

public class SaltoDoble : MonoBehaviour
{
    public Rigidbody rb;
    public float saltover;
    private bool EnElSuelo = true;
    public int maxSaltos = 2;
    public int saltoActual = 0;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && (EnElSuelo || maxSaltos > saltoActual))
        {
            rb.velocity = new Vector3(0f, saltover, 0f * Time.deltaTime);
            EnElSuelo = false;
            saltoActual++;
        }


    }
    private void OnCollisionEnter(Collision collision)
    {
        EnElSuelo = true;
        saltoActual = 0;
    }














}
